# @picter/frontier

Template based static website generator with features for dynamic content during generation and post processing.

[![build status](https://gitlab.com/picter/frontier/badges/master/build.svg)](https://gitlab.com/picter/frontier/commits/master)

## Usage

    Usage: frontier [options] [command]


    Commands:

      build       Builds your page and places result in output path.
      serve       Builds and serves your page with live reload.
      help [cmd]  display help for [cmd]

    Options:

      -h, --help     output usage information
      -V, --version  output the version number

## Folder structure

    templates/
    source/
      styles/index.styl
      assets/
      00-template-based.json
      10-some-markdown-content.md
      index.ini

### Templates folder

Contains handlebar templates to be consumed by json files within the source folder.

Template helpers: There are just the default handlebar helpers plus and extra svg helper to inline svg files.

### Source folder

This folder contains all the sources for the website to be generated. The required files and folders are listed above in the overview.

You can nest content (almost) infinite. Just make sure to include all required files and folders into each subfolder.

Content files need to be prefixed with a digit to give them a order. This is mandatory. The format must be `<number>-<filename>.<extension>`.
