import * as Application from 'koa';
import * as livereload from 'livereload';
import * as path from 'path';
import * as send from 'koa-send';

import { renderPage } from './renderer';
import { renderStylesheet } from './styles';

export default (projectRoot: string) => {
  console.log(`@pages/frontier serves ${projectRoot}`);

  const lrserver = livereload.createServer({
    exts: ['json', 'md', 'styl', 'ini', 'hbs'],
  });
  lrserver.watch(projectRoot);

  const app = new Application();

  app.use(async (ctx) => {
    console.log(ctx.url);
    const url = path.join('source' + ctx.url);
    if (url.endsWith('styles.css')) {
      ctx.type = 'text/css';
      ctx.body = await renderStylesheet(
        projectRoot,
        path.join(projectRoot, url).replace('.css', '/index.styl'),
      );
    } else if (url.includes('assets') || url.includes('favicon.ico')) {
      await send(ctx, url, { root: projectRoot });
    } else if (!url.endsWith('/')) {
      ctx.redirect(ctx.url + '/');
    } else {
      try {
        ctx.body = await renderPage(projectRoot, path.join(projectRoot, url)) +
          `<script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
            ':35729/livereload.js?snipver=1"></' + 'script>')
          </script>`;
      } catch (err) {
        ctx.status = 500;
        ctx.body = err.message;
      }
    }
  });

  app.listen(3141);

  console.log(`@pages/frontier running at http://127.0.0.1:3141`);
};
