import * as handlebars from 'handlebars';
import * as moment from 'helper-moment';

import markdown from '../helpers/markdown';
import svg from '../helpers/svg';
import parseInt from '../helpers/parseInt';
import equal from '../helpers/equal';
import select from '../helpers/select';

export default (projectRoot) => {
  handlebars.registerHelper('markdown', markdown);
  handlebars.registerHelper('svg', svg(projectRoot));
  handlebars.registerHelper('moment', moment);
  handlebars.registerHelper('equal', equal);
  handlebars.registerHelper('select', select);
  handlebars.registerHelper('parseInt', parseInt);
  return handlebars;
};
