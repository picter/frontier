import * as fs from 'fs';
import * as path from 'path';

export const listTemplates = (projectRoot: string) => {
  const templateDirectory = path.join(projectRoot, 'templates');
  const allTemplates = fs.readdirSync(templateDirectory);

  const templates = allTemplates.filter(
    template => !['index.hbs'].includes(template),
  );

  return templates;
};
