import * as path from 'path';

export const getProjectRoot = args =>
  path.resolve(args.length > 0 ? args[0] : process.cwd());
