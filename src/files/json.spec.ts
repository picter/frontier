import { getIn, patchObject } from './json';

test('patchObject() is defined', () => {
  expect(patchObject).toBeDefined();
});

test('patchObject() return the same keys as original input', () => {
  const object = { a: 1, b: '${reference}', c: 3 };
  const result = patchObject(object, {});
  expect(Object.keys(result)).toEqual(['a', 'b', 'c']);
});

test('patchObject() return the value for each key as original input', () => {
  const object = { a: 1, b: 'some words' };
  const result = patchObject(object, {});
  expect(Object.values(result)).toEqual([1, 'some words']);
});

test('patchObject() to replace key names', () => {
  const object = { a: 1, '${reference}': 2, c: 3 };
  const context = { reference: 'b' };
  const result = patchObject(object, context);
  expect(Object.keys(result)).toEqual(['a', 'b', 'c']);
});

test('patchObject() to replace value names', () => {
  const object = { a: 1, b: '${reference}', c: 3 };
  const context = { reference: 'some words' };
  const result = patchObject(object, context);
  expect(result).toEqual({
    a: 1,
    b: 'some words',
    c: 3,
  });
});

test('patchObject() to replace keys and values by context', () => {
  const object = { '${a}': '${b}', '${c}': 1, c: 'hello ${d}' };
  const context = {
    a: 'a',
    b: 'this is VALUE',
    c: 'this is KEY',
    d: 'world',
  };
  const result = patchObject(object, context);
  expect(result).toEqual({
    a: 'this is VALUE',
    c: 'hello world',
    'this is KEY': 1,
  });
});


test('patchObject() to replace nested keys by context', () => {
  const object = { a: { '${ctx}': 1 } };
  const context = { ctx: 'b' };
  const result = patchObject(object, context);
  expect(result).toEqual({ a: { b: 1 } });
});

test('getIn() is defined', () => {
  expect(getIn).toBeDefined();
});

test('getIn() retrieves value of path from within plain object', () => {
  const object = { a: 1 };
  expect(getIn(object, 'a')).toEqual(1);
});

test('getIn() retrieves value of path from within nested object', () => {
  const object = { a: { b: { c: 1 } } };
  expect(getIn(object, 'a.b.c')).toEqual(1);
});
