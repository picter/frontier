import * as kramed from 'kramed';

import handlebars from '../handlebars';

export default (projectRoot, fileContent, context) => {
  const renderedMarkdown = kramed(fileContent);
  const templateHbs = handlebars(projectRoot).compile(
    `<div class="container markdown">${renderedMarkdown}</div>`,
  );
  return templateHbs(context);
};
