import * as fs from 'fs';
import * as path from 'path';

import handlebars from '../handlebars';

const replace = (string: string, context): string => {
  return string.replace(/\${[\w\.]+}/g, (finding) => {
    const name = finding.substr(2, finding.length - 3); // remove ${}
    const value = getIn(context, name);
    if (value) {
      return value;
    }
    return finding;
  });
};

export const getIn = (object: object, path: string) =>
  path.split('.').reduce((previousObject, pathSegment) => previousObject[pathSegment], object);

export const patchObject = (object = {}, context) => {
  const result = {};
  const keys = Object.keys(object);
  keys.forEach((key) => {
    switch (typeof object[key]) {
      case 'object':
        result[replace(key, context)] = patchObject(object[key], context);
        break;
      case 'string':
        result[replace(key, context)] = replace(object[key], context);
        break;
      default:
        result[replace(key, context)] = object[key];
        break;
    }
  });
  return result;
};

export const renderJsonFile = (projectRoot, template, content) => {
  const templatePath = path.join(projectRoot, 'templates', template + '.hbs');
  const templateContent = fs.readFileSync(templatePath, 'utf-8');
  const templateHbs = handlebars(projectRoot).compile(templateContent);
  return templateHbs(content);
};

export default (projectRoot, fileContent, context) => {
  const { template, content } = JSON.parse(fileContent);

  const contextAwareContent = patchObject(content, context);

  return renderJsonFile(projectRoot, template, contextAwareContent);
};
