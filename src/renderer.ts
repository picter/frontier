import * as fs from 'fs';
import * as handlebars from 'handlebars';
import * as ini from 'ini';
import * as mkdirp from 'mkdirp';
import * as ncp from 'ncp';
import * as path from 'path';
import * as winston from 'winston';
import axios from 'axios';

import json, { renderJsonFile } from './files/json';
import markdown from './files/markdown';
import { listTemplates } from './templates';
import { renderStylesheet } from './styles';

export const renderFile = (projectRoot, baseDirectory, file, context) => {
  const filePath = path.join(baseDirectory, file);

  if (!fs.existsSync(filePath) || !fs.statSync(filePath).isFile()) {
    throw new Error('File to render does not exist (' + filePath + ').');
  }

  const fileType = path.extname(file).replace('.', '');
  const fileName = path.basename(file, path.extname(file));
  const fileContent = fs.readFileSync(filePath, 'utf-8');

  winston.debug(fileName, fileType);

  switch (fileType) {
    case 'json':
      return json(projectRoot, fileContent, context);
    case 'md':
      return markdown(projectRoot, fileContent, context);
    default:
      throw new Error(`Unkown file extension: ${fileType}.`);
  }
};

const defaultOpts = {
  recursive: false,
  styles: false,
  assets: false,
  destination: undefined,
};

export const renderPage = async (projectRoot, baseDirectory, opts = defaultOpts) => {
  const allFiles = fs.readdirSync(baseDirectory);

  const ignoreFiles = ['assets', 'styles', 'index.ini'];
  const indexFile = path.join(baseDirectory, 'index.ini');

  const filteredFiles =  allFiles.filter(
    file => !ignoreFiles.includes(file),
  ).filter( // ignore files starting with dot
    file => !file.startsWith('.'),
  );

  // recursive rendering
  if (opts.recursive) {
    filteredFiles.map(
      file => path.join(baseDirectory, file),
    ).filter(
      filePath => fs.statSync(filePath).isDirectory(),
    ).forEach(async filePath => await renderPage(projectRoot, filePath, opts));
  }

  const files = filteredFiles.filter( // do not render directories
    file => fs.statSync(path.join(baseDirectory, file)).isFile(),
  );
  winston.debug('Files:', files);
  winston.debug('Has index file:', fs.existsSync(indexFile));

  winston.debug('Templates:', listTemplates(projectRoot));

  // Render
  const indexOfFile = filename => parseInt(filename.split('-')[0], 10);

  const indexFileContent = ini.parse(fs.readFileSync(indexFile, 'utf-8'));

  const requestUrl = indexFileContent.request;
  const context = {};
  if (requestUrl) {
    winston.debug('Request:', requestUrl);
    const result = await axios.get(requestUrl);
    context['request'] = result.data;
  }

  const content = files.sort((first, second) =>
    indexOfFile(first) - indexOfFile(second),
  ).map(
    file => renderFile(projectRoot, baseDirectory, file, context),
  ).join('\n');

  // Rendering context
  const indexContent = Object.assign(
    {},
    { content: new handlebars.SafeString(content) },
    indexFileContent,
  );

  const distDirectory = opts.destination || baseDirectory.replace('source', 'dist');
  console.log('Writing to', distDirectory);

  const distFile = path.join(distDirectory, 'index.html');

  const result = renderJsonFile(projectRoot, 'index', indexContent);

  mkdirp.sync(distDirectory);
  fs.writeFileSync(distFile, result, 'utf-8');

  if (opts.styles) {
    const css = await renderStylesheet(projectRoot, path.join(baseDirectory, 'styles/index.styl'));
    fs.writeFileSync(path.join(distDirectory, 'styles.css'), css, 'utf-8');
  }

  if (opts.assets) {
    ncp(path.join(baseDirectory, 'assets'), path.join(distDirectory, 'assets'));
  }

  return result;
};
