import * as autoprefixer from 'autoprefixer';
import * as fs from 'fs';
import * as postcss from 'postcss';
import * as stylus from 'stylus';

const prefixer = postcss([autoprefixer]);

const renderStylus = (projectRoot, fileName) => new Promise((resolve, reject) =>
  stylus(fs.readFileSync(fileName, 'utf-8'))
    .set('filename', fileName)
    .use(require('stylus-require-css-evaluator'))
    .include(projectRoot)
    .render((err, css) => {
      if (err) {
        return reject(err);
      }
      return resolve(css);
    }),
);

export const renderStylesheet = async (projectRoot: string, fileName) => {
  const styles = await renderStylus(projectRoot, fileName);

  return await prefixer.process(styles).css;
};
