import * as process from 'process';
import * as program from 'commander';

import { getProjectRoot } from '../utils/paths';

program
  .usage('[options] <path>')
  .parse(process.argv);

import server from '../server';

const projectRoot = getProjectRoot(program.args);

server(projectRoot);
