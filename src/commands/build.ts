import * as process from 'process';
import * as path from 'path';
import * as program from 'commander';
import * as winston from 'winston';

import { getProjectRoot } from '../utils/paths';
import { renderPage } from '../renderer';

winston.level = 'debug';

program
  .usage('[options] <path>')
  .option('-d, --destination <path>', 'Destination folder for build results')
  .parse(process.argv);

const projectRoot = getProjectRoot(program.args);

const baseDirectory = path.join(projectRoot, 'source');

renderPage(projectRoot, baseDirectory, {
  recursive: true,
  styles: true,
  assets: true,
  destination: program.destination,
});
