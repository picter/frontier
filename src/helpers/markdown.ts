import * as kramed from 'kramed';
import * as handlebars from 'handlebars';

export default (source, options) => {
  const result = kramed(source);
  return new handlebars.SafeString(result);
};
