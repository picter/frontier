export default (source, options) => {
  if (/^\d+$/.test(source)) {
    return parseInt(source, 10);
  }
  return source;
};
