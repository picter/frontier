import svg from './svg';

test('svg is an exported helper function', () => {
  expect(svg('/tmp')).toBeDefined();
});
