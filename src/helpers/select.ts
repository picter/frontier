export default (condition, choice1, choice2) => (condition ? choice1 : choice2);
